# coding:utf8
from .base import BaseHandler
from tornado.gen import coroutine, Return


class IndexHandler(BaseHandler):
    @coroutine
    def get(self, *args, **kwargs):
        ######从数据库取数据的逻辑：handler调用service --> service调用model -->model直接读取数据库
        # user = yield self.user_srv.get_user_by_id(1)
        # self.render('index.html', user=user)
        self.render('index.html')


class AppDataHandler(BaseHandler):
    @coroutine
    def get(self):
        # data = yield self.app_srv.get_app_data()
        data = []
        # 这段数据可从数据库取出，这里生成一段假数据
        for i in range(1, 100):
            data.append({"appname": 'app%s' % i, "download": i * 50})

        self.json_ok(data)
