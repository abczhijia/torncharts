DROP DATABASE IF EXISTS starter;
CREATE DATABASE starter
  DEFAULT CHARSET utf8;
USE starter;

CREATE TABLE `user` (
  `id`       INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `username` VARCHAR(20) UNIQUE          NOT NULL
)
  ENGINE = innodb
  DEFAULT CHARSET = utf8;

INSERT INTO `user` VALUES (NULL, "user1"), (NULL, "user2"), (NULL, "user3"), (NULL, "user4")