# coding:utf8
from __future__ import print_function
import tornado.ioloop
import tornado.web
from config.app_config import configs
from handler import index


def make_app():
    return tornado.web.Application([
        (r"/", index.IndexHandler),
        (r"/api/app/data", index.AppDataHandler)
    ], **configs)


if __name__ == "__main__":
    port = 8888
    app = make_app()
    app.listen(port)
    print("server is listening on http://localhost:%s" % port)
    tornado.ioloop.IOLoop.current().start()
