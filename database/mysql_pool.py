# coding:utf8

# from tornado_mysql import pools
from config.mysql_config import configs
from db_pool import db

async_db = db.DB(host=configs['host'], port=configs['port'], database=configs['db'], user=configs['user'],
                 password=configs['passwd'],
                 init_command="set names utf8")
